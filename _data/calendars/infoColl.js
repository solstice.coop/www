const EleventyFetch = require("@11ty/eleventy-fetch")
const ICAL = require("ical.js")

const { NEXTCLOUD_BASE_URL } = process.env
const now = new ICAL.Time.now()

module.exports = async function() {
  const options = {
    type: 'text',
    duration: '1d'
  }

  if(process.env.ELEVENTY_SERVERLESS) {
    options.duration = "0";
    options.directory = "cache";
  }

  const data = await EleventyFetch(`${NEXTCLOUD_BASE_URL}/remote.php/dav/public-calendars/W6TcJt9FPpAa27yp?export`, options)
  const cal = new ICAL.Component(ICAL.parse(data))

  return cal.getAllSubcomponents('vevent')
    .filter(component => {
      // Some Nextcloud calendars do not expose the 'status' property
      if (component.getFirstPropertyValue('status')) {
        return component.getFirstPropertyValue('status').toUpperCase() === 'CONFIRMED'
      }

      return true
    })
    .map(event => new ICAL.Event(event))
    .sort((a, b) => a.startDate.compare(b.startDate))
    .filter(event => event.startDate.compare(now) >= 0)
    .slice(0, 10)
};


