---
title: Programme des ateliers
permalink: /cooperative/programme/
eleventyExcludeFromCollections: true
scripts:
  - /js/controllers/redirect.js

# Adresse du fichier
location: https://solstice.coop/files/programme_ateliers_2023-2024.pdf
# Pour modifier le fichier, ouvrir
# https://gitlab.com/solstice.coop/www/-/blob/main/public/files/programme_ateliers_2023-2024.pdf
# et cliquer sur "Remplacer"

# Le script redirect.js renvoie automatiquement sur cette adresse
# Pour cela, il faut laisser le code suivant dans la page : **[{{ location }}]({{ location }}){ #locations }**
---

Le programme des ateliers peut être téléchargé à l'adresse **[{{ location }}]({{ location }}){ #location }**.
