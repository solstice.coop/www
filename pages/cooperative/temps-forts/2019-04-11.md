---
title: "Rencontre coopérative de printemps"
subtitle: 11 avril 2019
carousel:
  - img/temps-forts/2019-04-11/20190411_110137.jpg
  - img/temps-forts/2019-04-11/20190411_164732.jpg
  - img/temps-forts/2019-04-11/20190411_164734.jpg
  - img/temps-forts/2019-04-11/20190411_164740.jpg

---

Les rencontres coopératives sont proposées à tous les Solsticiens tous les semestres.

Ce sont des temps de rencontres mais aussi  des temps de travail collectifs au cours desquels
les échanges portent les réflexions et nourrissent les dynamiques entrepreneuriales de chaque Solsticien.

Cette fois-ci, nous avons cherché à **consolider la réflexion commerciale**.

Une matinée en plénière avec Brenda, Brandon et Mike et en trois temps :
- **Se former** pour dynamiser sa pensée entrepreneuriale et monter en compétences dans son parcours entrepreneurial : les nouvelles formations pensées et dédiées aux Solsticien.nes.
- **Partager** via les #CoopCafés : un format court & coopératif, à disposition des Solsticien.nes pour l’échange et la mise en avant de vos expertises.
- **Repérer les ressources et les espaces** : où trouver quoi et pourquoi ?!
  [Site de Solstice](https://solstice.coop/) et [Louty](https://appli.louty.net/)

Et cinq ateliers l’après-midi.

Ce fut, d’après vos nombreux retours, joyeux, riche et utile !

Prochaine rencontre coopérative en novembre 2019.
Merci pour vos présences et la richesses de nos échanges.
