---
seo:
  title: Solstice, coopérative d'activité et d'entrepreneuriat (CAE) dans la Drôme
eleventyNavigation:
  key: Accueil
  order: 1
figma: https://www.figma.com/file/cuBvusShQ85ecBMBIqwnlf/Site?node-id=116%3A378
---

# Communauté d’entrepreneur·es en Biovallée

Tester et péréniser son activité professionnelle en Vallée de la Drôme et dans le bassin Valentinois.
Solstice propose un accompagnement administratif et économique, une mise en commun de moyens et une gestion démocratique d'un même outil de travail\
… **tout en étant indépendant·e**.
{.lead}

{% include 'graphique-entrepreneures.html' %}

## Nous rejoindre en 3 étapes {.h5 .mt-3}

1. **découvrir le fonctionnement** des CAE lors d'une réunion d’information
1. **affiner mon projet** avec ma personne accompagnante
1. démarrer mon contrat

[Nous rejoindre](/nous-rejoindre){.btn .cta .mb-3}

## Être indépendant·e _et_ salarié·e

Le Contrat d'Appui au Projet d'Entreprise (<abbr>CAPE</abbr>) sert à tester une activité, à facturer dès le premier jour et à être accompagné·e pour ne pas se sentir seul·e.

Votre trésorerie vous le permet ?
On passe au Contrat Entrepreneur·e Salarié·e Associé·e (<abbr>CESA</abbr>).
C'est un <abbr title="Contrat à Durée Indéterminée">CDI</abbr>, des protections sociales, un salaire au besoin et pas de subordination à un·e patron·ne.

Votre projet est devenu mâture ?
C'est l'occasion de devenir associé·e, de participer et de voter
pour les orientations de notre coopérative.

### Outils et services mis en commun

Les dimensions administratives, comptables et juridiques sont réalisées par de vraies personnes qui vous répondront toujours : vous restez concentré·es sur votre cœur de métier.

[En savoir plus sur notre fonctionnement au quotidien](/cooperative/fonctionnement/){.btn .cta}

### Un organisme de formation certifié Qualiopi

![Processus certifié Qualiopi • République Française • Actions de formation et Bilans de compétences](img/logo_qualiopi.svg){.float-md-end .logo-qualiopi .mt-sm-n5}

Vous donnez des formations professionnelles ou effectuez des bilans de compétences ?
La [certification Qualiopi ?](https://travail-emploi.gouv.fr/formation-professionnelle/acteurs-cadre-et-qualite-de-la-formation-professionnelle/article/qualiopi-marque-de-certification-qualite-des-prestataires-de-formation){target="_blank"} vous permet de bénéficier du financement des <abbr title="Opérateurs de compétences">OPCO</abbr>.

[En savoir plus sur Solstice Organisme de formation](/formations/){.btn .cta}

[Consulter notre catalogue de formations](/formations/catalogue/){ .btn .cta }

## Une entreprise partagée, pionnière de la loi <abbr title="Économie Sociale et Solidaire">ESS</abbr>

Testez la **gestion démocratique d'entreprise** à votre rythme :
gestion transparente des comptes, un droit de vote par personne et pourquoi pas
construire les orientations stratégiques en rejoignant le Conseil d'Administration.


[Qu'est-ce qu'une CAE ?](https://www.les-cae.coop/qu-est-ce-qu-une-cae){target="_blank" .cta .btn}

{% include 'partials/_financeurs.njk' %}
