---
title: Organisme de formation
eleventyNavigation:
  key: Organisme de formation
  parent: Accueil
  order: 3
---

# Organisme de formation

![Processus certifié Qualiopi • République Française • Actions de formation et Bilans de compétences](img/logo_qualiopi.svg){.float-md-end .logo-qualiopi .mt-sm-n5}

Solstice SCOP est un organisme de formation certifié Référentiel National Qualité (Qualiopi) au titre des **Actions de formation** depuis le 1er octobre 2021 et au titre des **Bilans de compétence** depuis le 28 avril 2023.

Nous développons et portons des activités de Formation Professionnelle Continue avec une exonération de TVA et un accès aux fonds de financements publics de la formation.

[Consulter le certificat](/files/certificat_qualiopi.pdf)

<br class="clearfix">

### Taux de satisfaction • Chiffres 2023

**En 2023, 50 intervenants•es en formation ont réalisé un total de 38.983 heures-stagiaires et ont formé 2247 stagiaires.**

- **99%** des stagiaires sont satisfaits ou très satisfaits par leur formation.
- **100%** des stagiaires estiment avoir tout à fait atteint les objectifs de leur formation.
- **100%** des stagiaires sont satisfaits ou très satisfaits de la qualité de l'animation pédagogique
- **100%** des stagiaires sont satisfaits ou  très satisfaits de l'organisation de leur formation.

En 2023, 4 bilans de compétences ont été réalisés avec un niveau de satisfaction globale de 97.5%. 

## Vous êtes formatrice·teur ?

<div class="row">
<div class="col-12 col-md-4">

Solstice héberge **vos formations**.

[En savoir plus ? Contactez-nous !](/contact/#contact){ .btn .cta }
</div>

<div class="col">

Chaque année, une quarantaine d'entrepreneur·es solsticien·nes hébergent leur activité de formation dans la coopérative. Le **[catalogue](/formations/catalogue/)** proposé couvre une vingtaine de thématiques. La démarche de certification Qualiopi est portée par les entrepreneur·es formatrice·teurs et l'équipe support. l'objectif de cette démarche est de structurer l'exercice de ce métier pour répondre aux évolutions du marché de la Formation professionnelle continue. Tou·te·s les entrepreneur·es formatrice·teurs respectent le cahier des charges de l'OF Solstice. Ils·Elles sont formée·s et accompagné·es pour répondre aux exigences règlementaires et s'engagent dans une démarche commune d'amélioration continue.

Chaque **[formateur·trice](/formations/catalogue/)** est en charge de son portefeuille client·es, communique auprès de ses client·es (web, mail, plaquette…) et peut proposer des formations au **[catalogue](/formations/catalogue/)** ou sur mesure (à la demande, en fonction des besoins des clients), en présentiel et/ou à distance. Certains contenus et tarifs peuvent être personnalisables (notamment pour les Solsticien·nes).

</div>
</div>

## Vous cherchez une formation ?


<div class="row">
<div class="col-12 col-md-4">

Nos formateur·ices proposent **de nombreuses formations**.

[Consulter le catalogue](/formations/catalogue/){ .btn .cta }

</div>

<div class="col">

Quelques-unes des thématiques proposées :

- Accompagnement, relation d’aide, médiation
- Massages bien-être
- Communication inter-personnelle
- Compostage et gestion des déchets
- Data sciences, Intelligence artificielle
- Écriture, Communication écrite
- Gestion de projets participatifs, Intelligence collective
- Intervention en institutions, Pédagogie
- Langues, Lecture labiale, Langue des signes
- Management, Communication, RH
- Ostéopathie animale
- Psychologie, Psychothérapie, Psycho corporelle
- Techniques textiles
- Web, RGPD, Digital, Bureautique

</div>
</div>

## Vous cherchez un bilan de compétence ?

Le bilan de compétences permet de définir ou de préciser un projet professionnel, de trouver une orientation, de vérifier si votre projet est réalisable. Il permet d’être accompagné·e dans sa réflexion par un·e consultant·e dans un centre de bilan de compétences.** (Article L6313-4 du code du travail) 

Dans le cadre de cet accompagnement, nous vous proposons :  
- Un bilan individualisé, construit sur mesure en fonction de vos besoins et attentes, et mené par des coachs professionnel.les certifié.es et/ou accompagnant.es en bilan de compétences.
- D’envisager autrement votre parcours ou de consolider votre projet d’évolution, sous statut salarial, indépendant ou entrepreneurial.  
- De vous faire bénéficier de nos ressources et de nos expertises dans un grand nombre de secteurs d’activité.

Le bilan de compétence se déroule en 3 phases :  

La **phase préliminaire** aura pour objet de :
-  analyser la demande et le besoin du bénéficiaire,
-  déterminer le format le plus adapté à la situation et au besoin,
-  définir conjointement les modalités de déroulement du bilan.

Une **phase d’investigation** permettra :
- soit de construire son projet professionnel et d’en vérifier la pertinence
- soit d’élaborer une ou plusieurs alternatives.

Une **phase de conclusion,** par la voie d’entretiens personnalisés, permettra de :
- s’approprier les résultats détaillés de la phase d’investigation
- recenser les conditions et moyens favorisant la réalisation du ou des projets professionnels
- prévoir les principales modalités et étapes du ou des projets professionnels, dont la possibilité de bénéficier d’un entretien de suivi avec le prestataire de bilan.

Cette dernière phase se terminera par la présentation des résultats détaillés et la remise d’un document de synthèse.
A la fin de la prestation, il vous sera proposé d’évaluer l’ensemble de la prestation ainsi qu’après 6 mois de la fin de la prestation. Par ailleurs, à chaque séance et à tout moment vous êtes invité(e) à vous exprimer sur votre satisfaction et l’amélioration de la prestation.

### Outils mobilisés

- Mise à disposition d’un livret individuel de progression 
- Outils de coaching (photo-langage, blason, domaine de vie,…)
- Outil d’aides à l’orientation (logiciel Parcouréo, questionnaire métier, domaines de préférences,...
- Sites ressources pour développer ses compétences en autonomie 
- Elaboration validation et mise en œuvre d'un plan d'actions 
- Réalisation et restitution d'un document de synthèse

### Modalités

Chaque bilan a une **durée de 24h, dont au moins 13h d’entretiens** séquencées en séances individuelles de 1h30 ou 2h. 
- **Présentiel, à distance ou mixte**, selon l’offre du conseiller bilan et votre choix  
- **Plusieurs formules horaires disponibles** en fonction de vos objectifs et disponibilités  
- Toutes nos prestations sont réalisées **conformément au cadre règlementaire** du bilan de compétences

Nous vous garantissons que sur l’ensemble du processus d’accompagnement et plus précisément la nature des échanges au cours des entretiens sont strictement confidentiels et personnels et se réalisent en toute neutralité.


### Public

Salariés ou demandeurs d’emploi, entrepreneurs, professions indépendantes et artisans 
Tous secteurs d’activités : public, privé, associatif

- **Pré-requis :Le bilan de compétence requiert de la disponibilité, un engagement volontaire et une motivation dans la démarche.**
- Intervenant·es sensibles à l'accueil des personnes en situation de handicap, HPI. Nous consulter pour l'adaptabilité à votre situation.

### Délai d’accès
Varie en fonction du mode de financement. Nous contacter


### Prix et Financement

Le coût d’un Bilan de compétence est de 1950€. Différents modes de financement possibles : 

• CPF  
• Plan de développement des compétences  
• Pôle emploi  
• Financement direct

Pour plus d’informations, n’hésitez pas à nous contacter au 04 75 25 32 30

### Pour plus d’informations

• **Site du ministère du travail :**  
[https://travail-emploi.gouv.fr/formation-professionnelle/droit-a-la-formation-et-orientation-professionnelle/bilan-competences  
**•**](https://travail-emploi.gouv.fr/formation-professionnelle/droit-a-la-formation-et-orientation-professionnelle/bilan-competences%EF%BF%BC%E2%80%A2) **Arrêté du 31 juillet 2009 relatif au bilan de compétences des agents de l’État :**  
<https://www.legifrance.gouv.fr/loda>


**Deux intervenantes au sein de Solstice proposent des bilans de compétence.**

• Emilie Moret = <https://solstice.coop/entrepreneures/emilie-moret/>

• Agnès Culpo = <https://solstice.coop/entrepreneures/agnes-culpo/>


## Accessibilité aux personnes en situation de handicap
Pour toute information concernant nos conditions d'accès au public en situation de handicap ou en situation particulière méritant d'être mentionnée(locaux, adapatation des moyens de la prestation), merci de contacter notre référent handicap : Bertrand Barrot (bertrand.barrot(@)solstice.coop).

## Vous avez besoin d’être formé·e au métier de formateur·trice ?

<div class="row">
<div class="col-12 col-md-4">

Solstice vous propose une **formation en deux modules**.

[Vous souhaitez recevoir le programme ? Contactez-nous](/contact/#formation-formateurice){ .btn .cta }
</div>

<div class="col">

Que votre activité de formation soit hébergée ou non chez Solstice, cette formation est pour vous si vous êtes :
- formateur·trice occasionnel·le
- formatrice·teur confirmé·e
- nouvelle et nouveau formatrice·teur

**Pourquoi ?**
- Acquérir ou réviser les outils de base
- Revoir sa pratique sous un autre angle
- Mettre à jour ses connaissances sur la formation pro
- Animer efficacement une formation
- Structurer professionnellement son offre de formation (ingénierie)

</div>
</div>
