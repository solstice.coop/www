# Solstice.coop

Documentation de travail et code source du site web de la Coopérative d'Activité et d'Emploi Solstice.

**Raccourcis** :

- [🌎 Visualiser le site](https://solstice.coop/)
- [📨 ~~Travail à faire~~](https://gitlab.com/solstice.coop/www/-/boards)
- [⏳ Comptabiliser le temps passé](le-temps-qui-passe.csv)

## Actions communes

- [🫱🏻‍🫲🏼 Mettre à jour la liste accompagnant·es](_data/accompagnants.json)
- [🏢 Mettre à jour la page réglementaire des formations](pages/formations/index.md)
